#!/usr/bin/env node
import yargs from "yargs/yargs"
import { config } from "dotenv"
import { cdkDeploy } from "./lib/cdk"
import { dumpDotEnv } from "./lib/dot-env"
import { cd, run } from "./lib/run"

config()

const argv = yargs(process.argv.slice(2)).options({
  workdir: { type: "string", alias: "wd", demandOption: true },
  environment: { type: "string", alias: "env", demandOption: true },
  stack: { type: "string", alias: "st", demandOption: true },
  args: { type: "string" },
  dumpEnv: { type: "string", alias: "de" },
  dumpEnvSource: { type: "string", alias: "deSrc" },
  dumpEnvTarget: { type: "string", alias: "deTarget" },
  dumpEnvAll: { type: "boolean" },
  diff: { type: "boolean" },
  deploy: { type: "boolean" },
  bootstrap: { type: "boolean" },
  whatIf: { type: "boolean" },
  yarn: { type: "boolean" },
  npm: { type: "boolean" },
}).argv

if (argv.dumpEnv) {
  dumpDotEnv({
    ...argv,
    dumpEnv: argv.dumpEnv,
  })
}

if (argv.workdir) {
  cd(argv.workdir)
}

if (argv.yarn) {
  run("yarn")
}

if (argv.npm) {
  run("npm")
}

cdkDeploy({
  stack: argv.stack,
  diff: argv.diff,
  deploy: argv.deploy,
  bootstrap: argv.bootstrap,
  args: argv.args,
  whatIf: argv.whatIf,
})
