## Gitlab Runners Setup

# install gitlab runner

```
# deb only (use ubuntu image!!)
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

```

# Install docker

```
 sudo apt-get update
 sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin


```

# install docker machine gitlab fork

```
# curl -L https://gitlab-docker-machine-downloads.s3.amazonaws.com/master/docker-machine >/tmp/docker-machine &# & sudo install /tmp/docker-machine /usr/local/bin/docker-machine


curl -O "https://gitlab-docker-machine-downloads.s3.amazonaws.com/v0.16.2-gitlab.13/docker-machine-Linux-x86_64"
sudo cp docker-machine-Linux-x86_64 /usr/local/bin/docker-machine
sudo chmod +x /usr/local/bin/docker-machine
```

# test docker machine aws

```
docker-machine create --driver amazonec2 \
 --amazonec2-access-key xxx \
 --amazonec2-secret-key xxx \
 --amazonec2-region eu-central-1 \
 --amazonec2-instance-type t2.medium \
  --amazonec2-ami ami-08ca3fed11864d6bb \
 runner-test
docker-machine rm runner-test
```

# register gitlab runner

```
sudo gitlab-runner register \
 --non-interactive \
 --url "https://gitlab.com" \
 --locked=false \
 --registration-token "xxx" \
 --description "aws-job-runner" \
 --executor "docker+machine" \
 --docker-image alpine:latest
```

# update config

```
sudo nano /etc/gitlab-runner/config.toml
```

# restart runner

```
gitlab-runner restart
```

## RUNNER CONFIG DO

```
concurrent = 50
check_interval = 0

[session_server]
session_timeout = 1800

[[runners]]
name = "aws-job-runner"
url = "https://gitlab.com"
token = "__PREREGISTERED__"
executor = "docker+machine"
[runners.custom_build_dir]
[runners.cache]
Type = "s3"
Shared = true
[runners.cache.s3]
ServerAddress = "s3.amazonaws.com"
AccessKey = "yyyy"
SecretKey = "yyyy"
BucketName = "modus-gitlab-cache"
BucketLocation = "eu-central-1"
[runners.cache.gcs]
[runners.cache.azure]
[runners.docker]
image = "alpine"
privileged = true
disable_cache = true
[runners.machine]
IdleCount = 1
MaxBuilds = 10
MachineDriver = "amazonec2"
MachineName = "gitlab-docker-machine-%s"
MachineOptions = [
"amazonec2-access-key=xxx",
"amazonec2-secret-key=yyy",
"amazonec2-region=eu-west-1",
"amazonec2-use-private-address=true",
"amazonec2-tags=runner-manager-name,gitlab-aws-autoscaler,gitlab,true,gitlab-runner-autoscale,true",
"amazonec2-instance-type=t2.medium",
"amazonec2-vpc-id=xxxx",
"amazonec2-root-size=40",
"amazonec2-ami=ami-08ca3fed11864d6bb"
]

```

```
gitlab-runner --debug run
```

cache: https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnerscachegcs-section
