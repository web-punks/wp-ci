# Envs cli

It is a cli that helps keeps env variables for different application / environments through AWS Secrets Manager

## Usage

The cli creates secrets pages for every application service

### Provision

It provision secret pages needed for a service. Ex:

```
yarn invoke:envs provision --appId=myApp --service=backend --environment=local
yarn invoke:envs provision --appId=myApp --service=backend --environment=local
yarn invoke:envs provision --appId=myApp --service=backend --environment=prod
```

### Set

It sets a secret value

```
# environment scoped secret
yarn invoke:envs set --appId=myApp --environment=local --key=KEY1 --value=value1

# service scoped secret
yarn invoke:envs set --appId=myApp --environment=local --service=backend --key=KEY2 --value=value2
```

### Get

It reads a secret value:

```
yarn invoke:envs get --appId=myApp --environment=local --key=KEY1
```

### Get App

It reads a secret values page:

```
yarn invoke:envs get-all --appId=myApp --environment=local
yarn invoke:envs get-all --appId=myApp --environment=local --service=backend
```

### Delete

It deletes a secret value:

```
yarn invoke:envs remove --appId=myApp --environment=local --key=KEY1
yarn invoke:envs remove --appId=myApp --environment=local --service=backend --key=KEY2
```

### Pull

It pulls a .env file

```
yarn invoke:envs pull --appId=myApp --environment=local --service=backend --outputFile=.env.test
```

### Push

It tokenize and pushes a .env file

```
yarn invoke:envs push --appId=myApp --environment=local --service=backend --inputFile=.env.test
```

### Compose

It compose a .env file using a local .env.template files putting the secret variables values into it

```
yarn invoke:envs compose --appId=myApp --environment=local --service=backend --outputFile=.env.test2 --templateFile=.env.test.template
```
