export interface GetDockerImageName {
  registry: string
  image: string
  environment: string
  suffix: string
}

export const getDockerImageName = (input: GetDockerImageName) =>
  `${input.registry}/${input.image}:${input.environment}${
    input.suffix ? `-${input.suffix}` : ""
  }`
