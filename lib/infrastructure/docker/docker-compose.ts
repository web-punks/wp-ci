import { run } from "../../run"

const dcBaseCommand = (environment: string) =>
  `docker-compose -f docker-compose.yml -f docker/docker-compose.ci.yml ${
    environment ? `-f docker/docker-compose.${environment}.yml` : ""
  }`

export const dc = (params: {
  environment: string
  cmd: string
  args?: string
  service: string
  whatIf: boolean
}) =>
  run(
    `${dcBaseCommand(params.environment)} ${params.cmd} ${params.service} ${
      params.args ? `${params.args}` : ""
    }`,
    params.whatIf
  )

const dcEnvConfig = (environment: string) => {
  const raw = run(`${dcBaseCommand(environment)} config`)
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const yaml = require("js-yaml")
  return yaml.safeLoad(raw)
}

export const dcSvcConfig = (environment: string, service: string) =>
  dcEnvConfig(environment).services[service]
