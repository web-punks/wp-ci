import {
  SecretsManagerClient,
  CreateSecretCommand,
  GetSecretValueCommand,
  DescribeSecretCommand,
  PutSecretValueCommand,
} from "@aws-sdk/client-secrets-manager"

export interface SecretValues {
  [key: string]: string
}

export interface SecretTag {
  key: string
  value: string
}

const getClient = () =>
  new SecretsManagerClient({
    region: process.env.AWS_REGION,
    credentials:
      process.env.AWS_ACCESS_KEY_ID && process.env.AWS_SECRET_ACCESS_KEY
        ? {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
          }
        : undefined,
  })

const parseSecretString = (secretString: string) =>
  JSON.parse(secretString) as SecretValues

const serializeSecretString = (secrets: SecretValues) => JSON.stringify(secrets)

const getSecretValue = ({
  key,
  secretString,
}: {
  secretString: string
  key: string
}) => {
  const secretBody = parseSecretString(secretString)
  return secretBody[key]
}

const setSecretValue = ({
  key,
  secretString,
  value,
}: {
  secretString: string
  key: string
  value: string
}) => {
  const secretBody = parseSecretString(secretString)
  secretBody[key] = value
  return serializeSecretString(secretBody)
}

const removeSecretValue = ({
  key,
  secretString,
}: {
  secretString: string
  key: string
}) => {
  const secretBody = parseSecretString(secretString)
  delete secretBody[key]
  return serializeSecretString(secretBody)
}

export const secretGet = async (name: string) => {
  const secret = await getClient().send(
    new DescribeSecretCommand({ SecretId: name })
  )

  const result = await getClient().send(
    new GetSecretValueCommand({ SecretId: secret.ARN })
  )
  if (!result) {
    throw new Error(`Secret ${name} not found`)
  }
  return result
}

export const secretExists = async (name: string) => {
  try {
    const secret = await getClient().send(
      new DescribeSecretCommand({ SecretId: name })
    )
    return !!secret
  } catch (e) {
    if ((e as any).name === "ResourceNotFoundException") {
      return false
    }
    throw e
  }
}

export const secretCreate = async ({
  name,
  description,
  tags,
}: {
  name: string
  description?: string
  tags?: SecretTag[]
}) => {
  await getClient().send(
    new CreateSecretCommand({
      Name: name,
      Description: description,
      Tags: tags?.map((x) => ({ Key: x.key, Value: x.value })),
      SecretString: "{}",
    })
  )
}

export const secretValueDelete = async ({
  name,
  key,
}: {
  name: string
  key: string
}) => {
  const secret = await secretGet(name)
  if (!secret?.ARN) {
    throw new Error(`Secret ${name} not found`)
  }

  const secretString = removeSecretValue({
    secretString: secret.SecretString ?? "{}",
    key,
  })

  await getClient().send(
    new PutSecretValueCommand({
      SecretId: secret.ARN,
      SecretString: secretString,
    })
  )
}

export const secretValueSet = async ({
  name,
  key,
  value,
}: {
  name: string
  key: string
  value: string
}) => {
  const secret = await secretGet(name)
  if (!secret?.ARN) {
    throw new Error(`Secret ${name} not found`)
  }

  const secretString = setSecretValue({
    secretString: secret.SecretString ?? "{}",
    key,
    value,
  })

  await getClient().send(
    new PutSecretValueCommand({
      SecretId: secret.ARN,
      SecretString: secretString,
    })
  )
}

export const secretValueGet = async ({
  name,
  key,
}: {
  name: string
  key: string
}) => {
  const secret = await secretGet(name)
  if (!secret?.ARN) {
    throw new Error(`Secret ${name} not found`)
  }

  const result = await getClient().send(
    new GetSecretValueCommand({
      SecretId: secret.ARN,
    })
  )
  return getSecretValue({
    secretString: result?.SecretString ?? "{}",
    key,
  })
}

export const secretValuesGet = async ({ name }: { name: string }) => {
  const secret = await secretGet(name)
  if (!secret?.ARN) {
    throw new Error(`Secret ${name} not found`)
  }

  const result = await getClient().send(
    new GetSecretValueCommand({
      SecretId: secret.ARN,
    })
  )
  return parseSecretString(result?.SecretString ?? "{}")
}

export const secretRawStringValueGet = async ({ name }: { name: string }) => {
  const secret = await secretGet(name)
  if (!secret?.ARN) {
    throw new Error(`Secret ${name} not found`)
  }

  const result = await getClient().send(
    new GetSecretValueCommand({
      SecretId: secret.ARN,
    })
  )
  return result?.SecretString ?? ""
}

export const secretValuesSet = async ({
  name,
  values,
}: {
  name: string
  values: SecretValues
}) => {
  const secret = await secretGet(name)
  if (!secret?.ARN) {
    throw new Error(`Secret ${name} not found`)
  }

  const secretString = serializeSecretString(values)
  await getClient().send(
    new PutSecretValueCommand({
      SecretId: secret.ARN,
      SecretString: secretString,
    })
  )
}

export const secretStringSet = async ({
  name,
  rawText,
}: {
  name: string
  rawText: string
}) => {
  const secret = await secretGet(name)
  if (!secret?.ARN) {
    throw new Error(`Secret ${name} not found`)
  }

  await getClient().send(
    new PutSecretValueCommand({
      SecretId: secret.ARN,
      SecretString: rawText,
    })
  )
}
