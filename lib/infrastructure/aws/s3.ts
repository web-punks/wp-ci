import {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
  HeadObjectCommand,
} from "@aws-sdk/client-s3"
import { createReadStream, createWriteStream } from "fs"
import { logMessage } from "../../utils/log"

export interface BucketObjectExists {
  path: string
  bucket: string
  region?: string
}

export const bucketObjectExists = async (
  input: BucketObjectExists
): Promise<boolean> => {
  try {
    const client = new S3Client({
      region: input.region,
    })
    const result = await client.send(
      new HeadObjectCommand({
        Bucket: input.bucket,
        Key: input.path,
      })
    )
    return result.$metadata.httpStatusCode === 200
  } catch (e) {
    if ((e as any)?.$metadata?.httpStatusCode === 404) {
      return false
    }
    throw e
  }
}

export interface BuckedUploadFileInput {
  file: string
  path: string
  bucket: string
  region?: string
}

export const bucketUploadFile = async (input: BuckedUploadFileInput) => {
  const client = new S3Client({
    region: input.region,
  })
  logMessage(`S3 upload -> ${input.file} -> ${input.path}`)
  await client.send(
    new PutObjectCommand({
      Bucket: input.bucket,
      Key: input.path,
      Body: createReadStream(input.file),
    })
  )
}

export interface BuckedDownloadFileInput {
  path: string
  bucket: string
  file: string
  region?: string
}

export const buckedDownloadFile = async (input: BuckedDownloadFileInput) => {
  const client = new S3Client({
    region: input.region,
  })
  logMessage(`S3 download -> ${input.path}`)
  const result = await client.send(
    new GetObjectCommand({
      Bucket: input.bucket,
      Key: input.path,
    })
  )
  await new Promise<void>((resolve, reject) => {
    result.Body.pipe(createWriteStream(input.file))
      .on("error", (err: any) => reject(err))
      .on("close", () => resolve())
  })
}
