import { readFileSync, writeFileSync } from "fs"
import { unzip, zip } from "./utils/7z"
import { calculateMd5File } from "./utils/fileHash"
import {
  buckedDownloadFile,
  bucketObjectExists,
  bucketUploadFile,
} from "./infrastructure/aws/s3"
import { replaceAll } from "./utils/string"
import { ensureDirectory, getDirectoryPath } from "./utils/directory"

interface CacheManifestItem {
  filename: string
  bucketPath: string
  folder: string
}

interface CacheManifestMetadata {
  timestamp: string
}

interface CacheManifest {
  items: CacheManifestItem[]
  metadata: CacheManifestMetadata
}

const MANIFEST_FILENAME = "manifest.json"

const createManifest = (path: string, items: CacheManifestItem[]) => {
  const content = JSON.stringify({
    items,
    metadata: {
      timestamp: new Date().toISOString(),
    },
  } as CacheManifest)

  ensureDirectory(getDirectoryPath(path))
  writeFileSync(path, content, "utf-8")
}

const readManifest = (path: string): CacheManifest => {
  const content = readFileSync(path, "utf-8")
  return JSON.parse(content)
}

const buildCacheFolder = (prefix: string, hash: string) =>
  `ci-cache/${prefix}/${hash}`

export const createLockFilesHash = async (files: string[]) => {
  return (await Promise.all(files.map((x) => calculateMd5File(x)))).join("|")
}

interface PullCacheParams {
  cachePrefix: string
  cacheHash: string
}

const cacheBucket = () => {
  if (!process.env.CI_CACHE_BUCKET) {
    throw new Error(`Missing CI_CACHE_BUCKET variable`)
  }
  return process.env.CI_CACHE_BUCKET
}

const cacheBucketRegion = () => process.env.CI_CACHE_BUCKET_REGION

const bucketParams = () => ({
  bucket: cacheBucket(),
  region: cacheBucketRegion(),
})

export interface PullCacheResult {
  found: boolean
}

const pullCacheItem = async (item: CacheManifestItem) => {
  await buckedDownloadFile({
    ...bucketParams(),
    path: item.bucketPath,
    file: item.filename,
  })
  await unzip({
    source: item.filename,
    target: item.folder,
  })
}

export const pullCache = async (
  input: PullCacheParams
): Promise<PullCacheResult> => {
  const cacheFolder = buildCacheFolder(input.cachePrefix, input.cacheHash)
  const manifestPath = `${cacheFolder}/${MANIFEST_FILENAME}`
  if (
    !(await bucketObjectExists({
      ...bucketParams(),
      path: manifestPath,
    }))
  ) {
    return {
      found: false,
    }
  }

  await buckedDownloadFile({
    ...bucketParams(),
    path: manifestPath,
    file: MANIFEST_FILENAME,
  })

  const manifest = readManifest(MANIFEST_FILENAME)
  await Promise.all(manifest.items.map((x) => pullCacheItem(x)))

  return {
    found: false,
  }
}

interface PushCacheParams {
  cacheHash: string
  cachePrefix: string
  folders: string[]
}

const pushCacheFile = async (sourceFile: string, path: string) => {
  await bucketUploadFile({
    ...bucketParams(),
    file: sourceFile,
    path,
  })
}

const pushCacheFolder = async (
  sourceFolder: string,
  cacheFolder: string
): Promise<CacheManifestItem> => {
  const zipTarget = `${replaceAll(sourceFolder, "/", "-")}.7z`
  const path = `${cacheFolder}/${zipTarget}`
  await zip({
    source: sourceFolder,
    target: zipTarget,
  })
  await pushCacheFile(zipTarget, path)
  return {
    folder: sourceFolder,
    bucketPath: path,
    filename: zipTarget,
  }
}

export const pushCache = async (input: PushCacheParams) => {
  const cacheFolder = buildCacheFolder(input.cachePrefix, input.cacheHash)
  const items = await Promise.all(
    input.folders.map((f) => pushCacheFolder(f, cacheFolder))
  )
  createManifest(MANIFEST_FILENAME, items)
  await pushCacheFile(MANIFEST_FILENAME, `${cacheFolder}/${MANIFEST_FILENAME}`)
}
