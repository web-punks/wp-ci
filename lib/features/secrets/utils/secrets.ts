import { secretExists } from "../../../infrastructure/aws/secrets-manager"

export const ensureSecretExists = async (name: string) => {
  const exists = await secretExists(name)
  if (!exists) {
    throw new Error(`Secret ${name} does not exist`)
  }
}
