import {
  TokenizedFile,
  TokenizedRowType,
  tokenizeEnvFile,
  stringifyEnvFile,
} from "./tokenize"

describe("file tokenizations", () => {
  it("should tokenize a file", () => {
    const result = tokenizeEnvFile(`KEY1=
KEY2=

# AA BB CC

KEY3=defaultkey
KEY4=
    `)

    expect(result).toEqual<TokenizedFile>({
      rows: [
        { key: "KEY1", value: "${KEY1}", type: TokenizedRowType.Variable },
        { key: "KEY2", value: "${KEY2}", type: TokenizedRowType.Variable },
        { key: "", value: "", type: TokenizedRowType.Other },
        { key: "# AA BB CC", value: "", type: TokenizedRowType.Other },
        { key: "", value: "", type: TokenizedRowType.Other },
        { key: "KEY3", value: "${KEY3}", type: TokenizedRowType.Variable },
        { key: "KEY4", value: "${KEY4}", type: TokenizedRowType.Variable },
        { key: "    ", value: "", type: TokenizedRowType.Other },
      ],
      variables: [
        { name: "KEY1", value: "" },
        { name: "KEY2", value: "" },
        { name: "KEY3", value: "defaultkey" },
        { name: "KEY4", value: "" },
      ],
    })
  })

  it("should stringify a file", () => {
    const file = stringifyEnvFile({
      rows: [
        { key: "KEY1", value: "${KEY1}", type: TokenizedRowType.Variable },
        { key: "KEY2", value: "${KEY2}", type: TokenizedRowType.Variable },
        { key: "", value: "", type: TokenizedRowType.Other },
        { key: "# AA BB CC", value: "", type: TokenizedRowType.Other },
        { key: "", value: "", type: TokenizedRowType.Other },
        { key: "KEY3", value: "${KEY3}", type: TokenizedRowType.Variable },
        { key: "KEY4", value: "${KEY4}", type: TokenizedRowType.Variable },
        { key: "    ", value: "", type: TokenizedRowType.Other },
      ],
      variables: [
        { name: "KEY1", value: "val1" },
        { name: "KEY2", value: "" },
        { name: "KEY3", value: "defaultkey" },
        { name: "KEY4", value: "otherKey" },
      ],
    })
    expect(file).toEqual(`KEY1=val1
KEY2=

# AA BB CC

KEY3=defaultkey
KEY4=otherKey
    `)
  })
})
