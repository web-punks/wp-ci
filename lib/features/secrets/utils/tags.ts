export const buildEnvironmentTag = (environment: string) => ({
  key: "environment",
  value: environment,
})
