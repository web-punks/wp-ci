import {
  AppSecretPage,
  AppSecretPageScope,
  AppSecretPageType,
} from "../../commands/types"
import { buildSecretPages } from "."

describe("secret pages", () => {
  it("should build correct page items", () => {
    const pages = buildSecretPages({
      appId: "app-id",
      environment: "dev",
      service: "service",
    })

    const expectedPages: AppSecretPage[] = [
      {
        appId: "app-id",
        environment: "dev",
        name: "app-id/dev-variables",
        scope: AppSecretPageScope.Environment,
        type: AppSecretPageType.Variables,
      },
      {
        appId: "app-id",
        environment: "dev",
        service: "service",
        name: "app-id/dev/service-variables",
        scope: AppSecretPageScope.Service,
        type: AppSecretPageType.Variables,
      },
      {
        appId: "app-id",
        environment: "dev",
        service: "service",
        name: "app-id/dev/service-template",
        scope: AppSecretPageScope.Service,
        type: AppSecretPageType.Template,
      },
    ]
    expect(pages).toEqual(expectedPages)
  })
})
