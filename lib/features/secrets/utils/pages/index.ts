import {
  AppSecretPage,
  AppSecretPageScope,
  AppSecretPageType,
  AppSecretsRef,
} from "../../commands/types"

const PATH_SEPARATOR = "/"

const buildEnvPageName = ({
  appId,
  environment,
  type,
}: {
  appId: string
  environment: string
  type: AppSecretPageType
}) => [appId, `${environment}-${type}`].join(PATH_SEPARATOR)

const buildServicePageName = ({
  appId,
  environment,
  service,
  type,
}: {
  appId: string
  environment: string
  service: string
  type: AppSecretPageType
}) => [appId, environment, `${service}-${type}`].join(PATH_SEPARATOR)

export const buildSecretPages = ({
  appId,
  environment,
  service,
}: AppSecretsRef): AppSecretPage[] => {
  const pages: AppSecretPage[] = [
    {
      name: buildEnvPageName({
        appId,
        environment,
        type: AppSecretPageType.Variables,
      }),
      appId,
      scope: AppSecretPageScope.Environment,
      type: AppSecretPageType.Variables,
      environment,
    },
    {
      appId,
      name: buildServicePageName({
        appId,
        environment,
        service,
        type: AppSecretPageType.Variables,
      }),
      scope: AppSecretPageScope.Service,
      type: AppSecretPageType.Variables,
      environment,
      service,
    },
    {
      appId,
      name: buildServicePageName({
        appId,
        environment,
        service,
        type: AppSecretPageType.Template,
      }),
      scope: AppSecretPageScope.Service,
      type: AppSecretPageType.Template,
      environment,
      service,
    },
  ]
  return pages
}

export const getSecretPageName = (input: {
  appId: string
  environment: string
  service?: string
  type: AppSecretPageType
}) => {
  if (input.service) {
    return buildServicePageName({
      appId: input.appId,
      environment: input.environment,
      service: input.service,
      type: input.type,
    })
  }

  return buildEnvPageName({
    appId: input.appId,
    environment: input.environment,
    type: input.type,
  })
}
