import { VariablesDict } from "../../types/variables"

export const parseEnvTemplateVariables = (content: string) => {
  const map: VariablesDict = {}
  content
    .split("\n")
    .filter((x) => x.trim() !== "" && x.indexOf("=") > 0)
    .forEach((x) => (map[x.split("=")[0]] = x.split("=")[1]))
  return map
}

export const stringifyTemplateVariables = (variables: VariablesDict) =>
  Object.keys(variables)
    .map((key) => `${key}=${variables[key]}`)
    .join("\n")
