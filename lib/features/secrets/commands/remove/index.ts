import { secretValueDelete } from "../../../../infrastructure/aws/secrets-manager"
import { getSecretPageName } from "../../utils/pages"
import { ensureSecretExists } from "../../utils/secrets"
import { AppSecretPageType } from "../types"
import { RemoveAppSecretInput } from "./types"

export const removeAppSecret = async (input: RemoveAppSecretInput) => {
  const pageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Variables,
  })
  await ensureSecretExists(pageName)
  await secretValueDelete({
    name: pageName,
    key: input.key,
  })
}
