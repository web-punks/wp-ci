export interface GetAppSecretInput {
  appId: string
  environment: string
  service?: string
  key: string
}
