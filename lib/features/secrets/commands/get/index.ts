import { secretValueGet } from "../../../../infrastructure/aws/secrets-manager"
import { getSecretPageName } from "../../utils/pages"
import { AppSecretPageType } from "../types"
import { GetAppSecretInput } from "./types"

export const getAppSecret = async (input: GetAppSecretInput) => {
  const pageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Variables,
  })
  return await secretValueGet({
    key: input.key,
    name: pageName,
  })
}
