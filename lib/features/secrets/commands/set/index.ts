import { secretValueSet } from "../../../../infrastructure/aws/secrets-manager"
import { getSecretPageName } from "../../utils/pages"
import { ensureSecretExists } from "../../utils/secrets"
import { AppSecretPageType } from "../types"
import { SetAppSecretInput } from "./types"

export const setAppSecret = async (input: SetAppSecretInput) => {
  const pageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Variables,
  })
  await ensureSecretExists(pageName)
  await secretValueSet({
    key: input.key,
    name: pageName,
    value: input.value,
  })
}
