import { readFileSync } from "fs"
import {
  secretStringSet,
  secretValuesGet,
  secretValuesSet,
} from "../../../../infrastructure/aws/secrets-manager"
import { getSecretPageName } from "../../utils/pages"
import {
  stringifyTemplateFile,
  tokenizeEnvFile,
} from "../../utils/tokenization/tokenize"
import { AppSecretPageType } from "../types"
import { PushAppSecretInput } from "./types"
import {
  createTokenizedFileWithoutVariables,
  processVariables,
} from "./variablesExtract"

const getPageNames = (input: PushAppSecretInput) => {
  const serviceTemplatePageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Template,
  })

  const serviceVariablesPageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Variables,
  })

  const envVariablesPageName = getSecretPageName({
    appId: input.appId,
    environment: input.environment,
    type: AppSecretPageType.Variables,
  })

  return {
    serviceTemplatePageName,
    serviceVariablesPageName,
    envVariablesPageName,
  }
}

const fetchSourceData = async (input: PushAppSecretInput) => {
  const pageNames = getPageNames(input)

  const serviceVariables = await secretValuesGet({
    name: pageNames.serviceVariablesPageName,
  })
  const environmentVariables = await secretValuesGet({
    name: pageNames.envVariablesPageName,
  })

  const envFileContent = readFileSync(input.inputFile, "utf8")

  return {
    pageNames,
    serviceVariables,
    environmentVariables,
    envFileContent,
  }
}

export const pushSecretsFile = async (input: PushAppSecretInput) => {
  const { serviceVariables, environmentVariables, envFileContent, pageNames } =
    await fetchSourceData(input)

  const tokenizedFile = tokenizeEnvFile(envFileContent)
  const outputVariables = processVariables(tokenizedFile, {
    serviceVariables,
    environmentVariables,
  })

  // update variables and file body
  await secretValuesSet({
    name: pageNames.serviceVariablesPageName,
    values: outputVariables.serviceVariables,
  })
  await secretValuesSet({
    name: pageNames.envVariablesPageName,
    values: outputVariables.environmentVariables,
  })

  const templateFile = createTokenizedFileWithoutVariables(tokenizedFile)
  await secretStringSet({
    name: pageNames.serviceTemplatePageName,
    rawText: stringifyTemplateFile(templateFile),
  })
}
