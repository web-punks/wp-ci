export interface PushAppSecretInput {
  appId: string
  environment: string
  service: string
  inputFile: string
}
