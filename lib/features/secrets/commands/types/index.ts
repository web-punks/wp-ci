export interface AppSecretsRef {
  appId: string
  service: string
  environment: string
}

export enum AppSecretPageScope {
  Environment = "environment",
  Service = "service",
}

export enum AppSecretPageType {
  Variables = "variables",
  Template = "template",
}

export interface AppSecretPage {
  appId: string
  name: string
  scope: AppSecretPageScope
  type: AppSecretPageType
  environment: string
  service?: string
}
