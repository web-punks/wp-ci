export interface PullAppSecretInput {
  appId: string
  environment: string
  service: string
  outputFile: string
}
