export interface ComposeAppSecretInput {
  appId: string
  environment: string
  service: string
  templateFile: string
  outputFile: string
}
