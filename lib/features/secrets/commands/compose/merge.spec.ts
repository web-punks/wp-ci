import { mergeTemplateFileVariables } from "./merge"

const inputFile = `
KEY1=
KEY2=
KEY3=defaultkey
KEY4=

`

describe("merge variables", () => {
  it("should merge variables", () => {
    const result = mergeTemplateFileVariables({
      templateContent: inputFile,
      serviceVariables: {
        KEY1: "servicevalue1",
      },
      environmentVariables: {
        KEY1: "envvalue1",
        KEY4: "environmentvalue4",
      },
    })

    expect(result).toBe(`KEY1=servicevalue1
KEY2=
KEY3=defaultkey
KEY4=environmentvalue4`)
  })
})
