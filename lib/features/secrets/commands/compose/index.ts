import { writeFileSync } from "fs"
import { secretValuesGet } from "../../../../infrastructure/aws/secrets-manager"
import { getContent } from "../../../../utils/files"
import { getSecretPageName } from "../../utils/pages"
import { AppSecretPageType } from "../types"
import { mergeTemplateFileVariables } from "./merge"
import { ComposeAppSecretInput } from "./types"
import { ensureDirectory, getDirectoryPath } from "../../../../utils/directory"

export const composeSecretsFile = async (input: ComposeAppSecretInput) => {
  const serviceVariablesPageName = getSecretPageName({
    ...input,
    type: AppSecretPageType.Variables,
  })
  const envVariablesPageName = getSecretPageName({
    appId: input.appId,
    environment: input.environment,
    type: AppSecretPageType.Variables,
  })

  const serviceVariables = await secretValuesGet({
    name: serviceVariablesPageName,
  })
  const environmentVariables = await secretValuesGet({
    name: envVariablesPageName,
  })

  const templateContent = getContent(input.templateFile)
  const result = mergeTemplateFileVariables({
    templateContent,
    serviceVariables,
    environmentVariables,
  })

  ensureDirectory(getDirectoryPath(input.outputFile))
  writeFileSync(input.outputFile, result, "utf8")
}
