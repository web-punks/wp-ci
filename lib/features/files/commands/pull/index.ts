import { writeFileSync } from "fs"
import { PullCustomFileInput } from "./types"
import { secretRawStringValueGet } from "../../../../infrastructure/aws/secrets-manager"
import { buildFileSecretName } from "../utils"
import { ensureDirectory, getDirectoryPath } from "../../../../utils/directory"

export const pullCustomFile = async (input: PullCustomFileInput) => {
  const fileSecretName = buildFileSecretName(input.ref)
  const fileContent = await secretRawStringValueGet({
    name: fileSecretName,
  })

  ensureDirectory(getDirectoryPath(input.outputFilePath))
  writeFileSync(input.outputFilePath, fileContent, "utf8")
}
