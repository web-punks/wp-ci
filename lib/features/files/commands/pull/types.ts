import { AppFileRef } from "../types"

export interface PullCustomFileInput {
  ref: AppFileRef
  outputFilePath: string
}
