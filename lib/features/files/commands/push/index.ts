import { readFileSync } from "fs"
import { secretStringSet } from "../../../../infrastructure/aws/secrets-manager"
import { buildFileSecretName } from "../utils"
import { FilePushInput } from "./types"

export const pushCustomFile = async (input: FilePushInput) => {
  const fileSecretName = buildFileSecretName(input.ref)
  const content = readFileSync(input.inputFilePath, "utf8")
  await secretStringSet({
    name: fileSecretName,
    rawText: content,
  })
}
