import { AppFileRef } from "../types"

export interface FilePushInput {
  ref: AppFileRef
  inputFilePath: string
}
