import { dumpEnv } from "./dump-env"

export interface DumpDotEnvInput {
  dumpEnv: string
  dumpEnvSource?: string
  dumpEnvTarget?: string
  environment: string
  service?: string
  dumpEnvAll?: boolean
}

export const dumpDotEnv = (input: DumpDotEnvInput) =>
  dumpEnv({
    folder: input.dumpEnv,
    sourceEnv: input.dumpEnvSource ? input.dumpEnvSource : ".env.template",
    targetEnv: input.dumpEnvTarget ? input.dumpEnvTarget : ".env.production",
    prefixes: [
      ...(input.environment ? [`${input.environment}_`] : []),
      ...(input.service ? [`${input.service}_`] : []),
      ...(input.environment && input.service
        ? [`${input.environment}_${input.service}_`]
        : []),
    ].filter((x) => x),
    all: input.dumpEnvAll,
  })
