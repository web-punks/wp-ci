export const jsonClone = <T>(obj: T): T => {
  return JSON.parse(JSON.stringify(obj))
}
