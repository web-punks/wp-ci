import { existsSync, mkdirSync } from "fs"
import { dirname } from "path"

export const getDirectoryPath = (filePath: string): string => {
  return dirname(filePath)
}

export const existsDirectory = (directoryPath: string) => {
  return existsSync(directoryPath)
}

export const ensureDirectory = (directoryPath: string) => {
  if (!existsDirectory(directoryPath)) {
    mkdirSync(directoryPath, { recursive: true })
  }
}
