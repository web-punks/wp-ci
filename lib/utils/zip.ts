import { zip } from "zip-a-folder"
import { logMessage } from "./log"

export const zipFolder = async (input: {
  targetFile: string
  sourceFolder: string
}) => {
  logMessage(`ZIP -> ${input.sourceFolder} -> ${input.targetFile}`)
  await zip(input.sourceFolder, input.targetFile)
}
