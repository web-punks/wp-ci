export const toSingleValueMap = <TItem, TKey, TValue>(
  values: TItem[],
  keySelector: (item: TItem) => TKey,
  valueSelector: (item: TItem) => TValue
): Map<TKey, TValue> => {
  const map = new Map<TKey, TValue>()
  values.forEach((item) => map.set(keySelector(item), valueSelector(item)))
  return map
}
