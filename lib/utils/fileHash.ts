import md5File from "md5-file"

export const calculateMd5File = (filename: string) => md5File(filename)
