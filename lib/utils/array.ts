export const range = (start: number, end: number): number[] => {
  return new Array(end - start + 1).fill(undefined).map((x, i) => i + start)
}

export const indexes = (startIndex: number, count: number) =>
  range(startIndex, startIndex + count - 1)

export interface Dict<TVal> {
  [key: string]: TVal
}

export const toDict = <TVal>(
  array: TVal[],
  selector: (value: TVal) => string
): Dict<TVal> => {
  const data: Dict<TVal> = {}
  for (const item of array) {
    data[selector(item)] = item
  }
  return data
}

export const toMap = <TKey, TVal>(
  array: TVal[],
  key: (val: TVal) => TKey
): Map<TKey, TVal> => {
  const map = new Map<TKey, TVal>()
  array.forEach((x) => map.set(key(x), x))
  return map
}
