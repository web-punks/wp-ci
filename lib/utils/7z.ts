import _7z from "7zip-min"
import { logMessage } from "./log"

export interface ZipInput {
  source: string
  target: string
}

export const zip = (input: ZipInput) => {
  logMessage(`7z - Zipping folder -> ${input.source} -> ${input.target}`)
  return new Promise<void>((resolve, reject) => {
    _7z.pack(input.source, input.target, (err) => {
      if (err) {
        reject(err)
        return
      }
      resolve()
    })
  })
}

export interface UnzipInput {
  source: string
  target: string
}

export const unzip = (input: ZipInput) => {
  logMessage(`7z - Unzipping folder -> ${input.source} -> ${input.target}`)
  return new Promise<void>((resolve, reject) => {
    _7z.unpack(input.source, input.target, (err) => {
      if (err) {
        reject(err)
        return
      }
      resolve()
    })
  })
}
