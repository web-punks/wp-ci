import { readFileSync, writeFileSync } from "fs"
import { join } from "path"
import { ensureDirectory, getDirectoryPath } from "./utils/directory"

interface VariablesDict {
  [key: string]: string | undefined
}

const parseVariables = (content: string) => {
  const map: VariablesDict = {}
  content
    .split("\n")
    .filter((x) => x.trim() !== "" && x.indexOf("=") > 0)
    .forEach((x) => (map[x.split("=")[0]] = x.split("=")[1]))
  return map
}

const extractVariables = (prefix: string) => {
  const variables: VariablesDict = {}
  Object.keys(process.env)
    .filter((x) => x.toUpperCase().startsWith(prefix.toUpperCase()))
    .forEach(
      (key) => (variables[key.substring(prefix.length)] = process.env[key])
    )
  return variables
}

const extractAllVariables = () => {
  const variables: VariablesDict = {}
  Object.keys(process.env).forEach((key) => (variables[key] = process.env[key]))
  return variables
}

const mergeVariables = (variables: VariablesDict, other: VariablesDict) => {
  const copy = JSON.parse(JSON.stringify(variables))
  Object.keys(other).forEach((key) => {
    if (copy[key] !== undefined) {
      copy[key] = other[key]
    }
  })
  return copy
}

const stringifyVariables = (variables: VariablesDict) =>
  Object.keys(variables)
    .map((key) => `${key}=${variables[key]}`)
    .join("\n")

const stringifyPythonVariables = (variables: VariablesDict) =>
  Object.keys(variables)
    .map((key) => `${key}="${variables[key]}"`)
    .join("\n")

const exportVariables = (variables: VariablesDict, format?: string) => {
  return format === "py"
    ? stringifyPythonVariables(variables)
    : stringifyVariables(variables)
}

const getAllPrefixes = (prefixes: string[]) => ["", ...prefixes]

export const dumpEnv = (params: {
  folder: string
  sourceEnv: string
  targetEnv: string
  all?: boolean
  prefixes?: string[]
}) => {
  let variables = parseVariables(
    readFileSync(join(params.folder, params.sourceEnv), "utf-8")
  )

  if (params.all) {
    variables = mergeVariables(variables, extractAllVariables())
  } else {
    for (const prefix of getAllPrefixes(params.prefixes ?? [])) {
      const processVars = extractVariables(prefix)
      variables = mergeVariables(variables, processVars)
    }
  }

  const filePath = join(params.folder, params.targetEnv)
  ensureDirectory(getDirectoryPath(filePath))
  writeFileSync(
    filePath,
    exportVariables(variables, params.targetEnv.split(".").pop())
  )
}
