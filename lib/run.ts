import * as sh from "shelljs"
import { logMessage } from "./utils/log"
sh.config.fatal = true
sh.config.verbose = true

export const cd = (dir: string) => {
  sh.cd(dir)
}

export const run = (command: string, whatIf = false) => {
  logMessage(`CMD -> ${command}`)
  if (whatIf) {
    return
  }

  const result = sh.exec(command)
  if (result.code !== 0) {
    throw new Error(
      `Error executing command ${command} -> code ${result.code}: ${result.stderr}`
    )
  }
  return result.stdout
}
