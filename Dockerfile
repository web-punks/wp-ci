FROM node:16-alpine as builder

COPY . /usr/src/app/wp-ci
WORKDIR /usr/src/app/wp-ci

RUN yarn install --frozen-lockfile && \
    yarn build

FROM node:16-alpine as dist

COPY --from=builder /usr/src/app/wp-ci/bin /usr/src/app/bin
COPY --from=builder /usr/src/app/wp-ci/node_modules /usr/src/app/bin/node_modules
WORKDIR /usr/src/app/bin
RUN apk add --no-cache py3-pip docker openrc && \
    pip3 install awscli && \
    echo 'alias dd="node /usr/src/app/bin/do.js"' >> ~/.bashrc && \
    echo 'alias cdk="node /usr/src/app/bin/cdk.js"' >> ~/.bashrc && \
    echo 'alias envs="node /usr/src/app/bin/envs.js"' >> ~/.bashrc && \
    echo 'alias mc="node /usr/src/app/bin/mc.js"' >> ~/.bashrc && \
    echo 'alias hh="node /usr/src/app/bin/help.js"' >> ~/.bashrc
