#!/usr/bin/env node
import yargs from "yargs/yargs"
import { config } from "dotenv"
import { createLockFilesHash, pullCache, pushCache } from "./lib/caching"
import { run } from "./lib/run"
import { logMessage } from "./lib/utils/log"

config()

const argv = yargs(process.argv.slice(2)).options({
  prefix: { type: "string", alias: "p", demandOption: true },
  lockFiles: { type: "string", alias: "lf", demandOption: true, array: true },
  folders: { type: "string", alias: "fold", demandOption: true, array: true },
  command: { type: "string", alias: "cmd", demandOption: true },
  force: { type: "boolean", alias: "f" },
}).argv

const handleCache = async () => {
  const cacheHash = await createLockFilesHash(argv.lockFiles)

  if (!argv.force) {
    logMessage(
      `Pulling cache 🚡🚡🚡🚡🚡🚡🚡🚡🚡🚡🚡🚡🚡 ${argv.prefix} -> ${cacheHash}`
    )
    const pullResult = await pullCache({
      cacheHash,
      cachePrefix: argv.prefix,
    })

    if (pullResult.found) {
      logMessage("Cache downloaded 🚂🚂🚂🚂🚂🚂🚂🚂🚂🚂")
      return
    }
  }

  logMessage("Cache miss 🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧")
  run(argv.command)

  await pushCache({
    cacheHash,
    cachePrefix: argv.prefix,
    folders: argv.folders,
  })

  logMessage("Cache pushed 🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀")
}

handleCache()
