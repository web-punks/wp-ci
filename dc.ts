#!/usr/bin/env node
import yargs from "yargs/yargs"
import { config } from "dotenv"
import { ecrLogin } from "./lib/infrastructure/aws/ecr"
import { getImageTag, pushImage, tagImage } from "./lib/docker"
import { dc, dcSvcConfig } from "./lib/infrastructure/docker/docker-compose"
import { dumpDotEnv } from "./lib/dot-env"
import { run } from "./lib/run"
import { logMessage } from "./lib/utils/log"

config()

const argv = yargs(process.argv.slice(2)).options({
  service: { type: "string", alias: "svc", demandOption: true },
  environment: { type: "string", alias: "env", demandOption: true },
  build: { type: "boolean" },
  pull: { type: "boolean" },
  push: { type: "string" },
  run: { type: "string" },
  dumpEnv: { type: "string", alias: "de" },
  dumpEnvSource: { type: "string", alias: "deSrc" },
  dumpEnvTarget: { type: "string", alias: "deTarget" },
  dumpEnvAll: { type: "boolean" },
  copy: { type: "string", alias: "cp" },
  commitHash: {
    type: "string",
    default: process.env.CI_COMMIT_SHA || run("git rev-parse HEAD"),
  },
  whatIf: { type: "boolean", default: false },
}).argv

if (argv.dumpEnv) {
  dumpDotEnv({
    ...argv,
    dumpEnv: argv.dumpEnv,
  })
}

logMessage(`Commit hash ${argv.commitHash}`)

if (argv.pull || argv.build) {
  dc({
    cmd: "pull",
    environment: argv.environment,
    service: argv.service,
    whatIf: argv.whatIf,
  })
}

if (argv.build) {
  dc({
    cmd: "build",
    environment: argv.environment,
    service: argv.service,
    whatIf: argv.whatIf,
  })
  dc({
    cmd: "push",
    environment: argv.environment,
    service: argv.service,
    whatIf: argv.whatIf,
  })
}

if (argv.run) {
  const commands = Array.isArray(argv.run) ? argv.run : [argv.run]
  for (const command of commands) {
    dc({
      cmd: "run --rm",
      environment: argv.environment,
      service: argv.service,
      args: command.trim(),
      whatIf: argv.whatIf,
    })
  }
}

if (argv.copy) {
  const pathsList = Array.isArray(argv.copy) ? argv.copy : [argv.copy]
  for (const paths of pathsList) {
    dc({
      cmd: "run --rm",
      environment: argv.environment,
      service: argv.service,
      args: `cp -r ${paths.split(" ")[0]} ${paths.split(" ")[1]}`,
      whatIf: argv.whatIf,
    })
  }
}

if (argv.push) {
  if (!argv.commitHash) {
    throw new Error("Cannot find CI_COMMIT_SHA env variable")
  }
  ecrLogin()
  const imageName = dcSvcConfig(argv.environment, argv.service).image
  logMessage("Source image ->", imageName)
  const ecrImageName = `${process.env.CI_AWS_ECR_REGISTRY}/${
    argv.push
  }:${getImageTag(imageName)}-${argv.commitHash}`
  tagImage(imageName, ecrImageName)
  pushImage(ecrImageName)
}
