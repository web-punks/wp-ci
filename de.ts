#!/usr/bin/env node
import yargs from "yargs/yargs"
import { config } from "dotenv"
import { dumpDotEnv } from "./lib/dot-env"

config()

const argv = yargs(process.argv.slice(2)).options({
  service: { type: "string", alias: "svc", demandOption: true },
  environment: { type: "string", alias: "env", demandOption: true },
  dumpEnv: { type: "string", alias: "de", demandOption: true },
  dumpEnvSource: { type: "string", alias: "deSrc" },
  dumpEnvTarget: { type: "string", alias: "deTarget" },
  dumpEnvAll: { type: "boolean" },
}).argv

dumpDotEnv(argv)
